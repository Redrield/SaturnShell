# SaturnShell
Library of wrappers and year agnostic code for FRC team 4069

__Master Build Status__ [![Master Build Status](https://travis-ci.org/Redrield/SaturnShell.svg?branch=master)](https://travis-ci.org/Redrield/SaturnShell)

__Command build status__ [![Command Build Status](https://travis-ci.org/Redrield/SaturnShell.svg?branch=command)](https://travis-ci.org/Redrield/SaturnShell)
