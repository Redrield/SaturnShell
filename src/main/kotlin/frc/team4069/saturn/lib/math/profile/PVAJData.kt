package frc.team4069.saturn.lib.math.profile

data class PVAJData(
    var pos: Double = 0.0,
    var vel: Double = 0.0,
    var accel: Double = 0.0,
    var jerk: Double = 0.0
)