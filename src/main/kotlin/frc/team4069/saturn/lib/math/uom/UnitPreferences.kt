package frc.team4069.saturn.lib.math.uom

data class UnitPreferences(val sensorUnitsPerRotation: Int, val radius: Double)