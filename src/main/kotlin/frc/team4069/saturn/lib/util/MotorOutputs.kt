package frc.team4069.saturn.lib.util

data class MotorOutputs(val left: Double, val right: Double)
