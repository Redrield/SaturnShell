package frc.team4069.saturn.lib.util

interface Source<T> {
    val value: T
}