package frc.team4069.saturn.lib.lidar

data class LidarPoint(val angle: Double, val distance: Int, val signalStrength: Int)
