package frc.team4069.saturn.lib.lidar.packet

import frc.team4069.saturn.lib.lidar.ControlCode

abstract class IncomingPacket(val controlCode: ControlCode)