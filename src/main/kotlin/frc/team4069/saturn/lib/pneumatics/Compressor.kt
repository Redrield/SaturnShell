package frc.team4069.saturn.lib.pneumatics

import edu.wpi.first.wpilibj.Compressor


// Just to follow naming conventions in the lib, if I need to expand it I'll make it a proper class and nothing will break robot code side
typealias Compressor = Compressor
